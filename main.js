const express = require('express');
const cors = require('cors');
var bodyParser = require('body-parser')

var corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200,
};

const app = express();

app.use(cors(corsOptions));
app.use(bodyParser.json());

const testFolder = './resources/';
const realtimeFolder = './realtimeres/';

const fs = require('fs');

const vision = require('@google-cloud/vision');
// Creates a client
const client = new vision.ImageAnnotatorClient({
	keyFilename: 'APIkey.json'
});

// Vision AI requests
getFaceAnnotations = (file) => {
	const request = {image: {source: {filename: file}}};
	return client.faceDetection(request)
		.then(results => {
			console.log(results);
			return results[0].faceAnnotations;
		})
		.catch(e => {
			throw new Error(e);
		})
};

getLabelAnnotations = (file) => {
	const request = {image: {source: {filename: file}}};
	return client.labelDetection(request)
		.then(results => results[0].labelAnnotations)
		.catch(e => {
			throw new Error(e);
		})
};

// server side events

function sse(req, res) {
	let messageId = 0;

	const intervalId = fs.watch(realtimeFolder, (event, file) => {
		if (event === 'change') {
			if (file) {
				getFaceAnnotations(realtimeFolder + file).then(result => {
					if (result[0]) {
						let data = {
							joy: result[0].joyLikelihood,
							sorrow: result[0].sorrowLikelihood,
							anger: result[0].angerLikelihood,
							surprise: result[0].surpriseLikelihood,
						};
						res.write(`id: ${messageId}\n`);
						res.write(`data: ${JSON.stringify(data)}\n\n`);
						messageId += 1;
					}

				}).catch(e => console.log(e));
			}
		}
	});

	req.on('close', () => {
		// fs.close();
	});
}

// endpoints
app.listen(5000, '127.0.0.1', () => {
	console.log("App listening on port 5000");
});

app.get('/faceStats', (req, res) => {
	let promiseArray = [];
	fs.readdir(testFolder, async (err, files) => {
		files.forEach(async file => promiseArray.push(getFaceAnnotations(testFolder + file)));
		await Promise.all(promiseArray).then(labels => res.send(labels)).catch(e => res.send(e));
	});
});

app.get('/labelStats', (req, res) => {
	let promiseArray = [];
	fs.readdir(testFolder, async (err, files) => {
		files.forEach(async file => promiseArray.push(getLabelAnnotations(testFolder + file)));
		await Promise.all(promiseArray).then(labels => res.send(labels)).catch(e => res.send(e));
	});
});

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/event-stream', (req, res) => {
	// SSE Setup
	res.writeHead(200, {
		'Content-Type': 'text/event-stream',
		'Cache-Control': 'no-cache',
		'Connection': 'keep-alive',
	});
	res.write('\n');

	sse(req, res);
});

app.post('/writeLog', (req, res) => {
	fs.appendFile("./logs", JSON.stringify(req.body.value) + ';', function(err) {
		if(err) {
			res.send(err);
			return console.log(err);
		}
		res.send('OK');
	});
});

app.get('/logs', (req, res) => {
	fs.readFile('./logs', 'utf-8', function(err, data) {
		if (err) {
			res.send(err);
			return console.log(err);
		}
		let logs = data.split(';');
		res.send(logs);
	})
});